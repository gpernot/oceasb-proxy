# Prérequis #

L'installation se fait sur un serveur Debian Bullseye.

Avant de déployer le proxy en utilisant Ansible, il faut installer les
dépendances nécessaires sur le serveur cible :

> Les commandes à effectuer sur le serveur sont préfixées de `target`,
> celles à réaliser sur le poste de l'opérateur sont préfixées de
> `operator`.

```
target# apt install python3
```

Téléchargement du playbook ansible :
```
operator$ git clone git@gitlab.com:gpernot/oceasb-proxy.git
```

Copie de la clef publique de l'opérateur dur le serveur :
```
operator$ ssh-copy-id -i ~/.ssh/id_rsa.pub root@target
```

# Déploiement de la configuration #

À la racine du dépôt git :
```
operator$ ansible-playbook -u root oceasb.yml -i hosts.yml
```

# Configuration #

## Configurations principales ##

Les configurations sont faites dans le fichier
`group_vars/proxies.yml`, et peuvent être affinées par hôte dans les
fichiers `host_vars/*`.

On y trouve les définitions suivantes:

### `high_priority_dst_domains` ###

Liste des domaines de destination dont le trafic sera le plus prioritaire.

### `low_priority_dst_domains` ###

Liste des domaines de destination dont le trafic sera le moins prioritaire.

### `high_priority_src_ip` ###

Liste des adresses IP source dont le trafic sera le plus prioritaire.

### `low_priority_src_ip` ###

Liste des adresses IP source dont le trafic sera le moins prioritaire.

### `denylist` ###

La liste des domaines supplémentaires bloqués par squidguard.

### `proxy_fqdn` ###

Le nom de domaine complet du proxy. Cette adresse est utilisée pour la
définition du virtual host apache (ou nginx) et le redirecteur squidguard.

### `webserver` ###

`nginx` ou `apache`.
Le serveur web à utiliser pour servir la page de blocage et Screen
Squid.

Seul `apache` permet l'authentification ldap pour sécuriser l'accès à
Screen Squid.

### `auth_ldap_url` ###

L'URL à utiliser pour accéder au serveur LDAP. _c.f._ [la documentation Apache](https://httpd.apache.org/docs/2.4/mod/mod_authnz_ldap.html#authldapurl).

### `auth_ldap_authorized_users` ###

La liste des opérateurs (`uid` LDAP) pouvant accéder à Screen Squid.

### `network_interface`  ###

Le nom de l'interface réseau utilisée par le proxy.

### `link_speed`  ###

La vitesse maximale d'émission vers les clients, en kilobit/s.

## Squidguard ##

Le fichier `roles/proxy/templates/squidGuard.conf.j2` peut être
modifié pour apporter des modifications fines à squidguard, notamment
pour les adresses sources (directive `src`) et les ACL (directive
`acl`). _c.f._ http://www.squidguard.org/Doc/configure.html

### Protocole HTTP ###

En cas de blocage, l'usager est redirigé vers la page personnalisable
`roles/proxy/files/blocked.html`.
Les paramètres de la requête suivants sont utilisables :
 - `clientaddr` : l'adresse IP d'origine de la requête
 - `clientdomain` : le nom de domain d'origine de la requête
 - `clientuser` : le nom de l'usager, en cas d'autentification au proxy
 - `targetgroup` : le nom de la denylist ayant entraîné le blocage
 - `url` : l'adresse demandée par l'usager

> Afin de permettre les redirections, nous utilisons une version
> personnalisée de squidguard dont le source se trouve à
> https://salsa.debian.org/gpernot/maintain_squidguard

### Protocole HTTPS ###

En cas de blocage d'une requête HTTPS, Squid empêche la connexion
mais ne sait pas, dans la configuration retenue, rediriger vers une
autre adresse. L'usager recevra donc simplement une « Connexion
échouée ».

## Screen Squid ##

Screen Squid compile les rapports d'utilisation du proxy. Ces rapports
sont consultables à l'adresse `http://proxy_fqdn/screensquid`.

> La version de Screen Squid utilisée est [une version
> personnalisée](https://gitlab.com/gpernot/screen-squid).

## Contrôle de trafic et QoS ##

La priorisation des flux est configurée sur l'interface
`network_interface`, avec un débit montant maximal de `link_speed`
kbit/s.

Le réglage fin des débits selon les classes de trafic peut être
effectué dans le fichier `roles/firewall/templates/tcstart.j2`.

Le marquage des paquets selon leur origine et/ou destination est
réalisé par squid (directive `tcp_outgoing_mark` dans
`roles/proxy/templates/squid.conf.j2`).

Trois classes de traffic sont définies :
 - `0x10`: trafic prioritaire
 - `0x20`: trafic normal
 - `0x30`: trafic basse priorité

Dans la configuration fournie, à pleine charge du réseau :
 - le trafic prioritaire a un débit maximal de 90% du `link_speed`
   (`link_speed // 1.1`).
 - le trafic par défaut a un débit maximal de 66% du `link_speed` (`link_speed // 1.5`).
 - le trafic de basse priorité a un débit maximal de 50% du
   `link_speed` (`link_speed // 2`).

# Tâches #

- [x] squid
  - [x] Script de déploiement
  - [x] Documentation
- [x] Screen Squid
  - [x] Script de déploiement
  - [x] Authentification
  - [x] Documentation
- [x] Squidguard
  - [x] Script de déploiement
  - [x] Mise à jour quotidienne des listes
  - [x] Page de blocage personalisée
  - [x] Documentation
- [x] Firewall
  - [x] Script de déploiement
  - [x] traffic control
  - [x] Documentation
- [x] Cahier d'exploitation
